#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   FPMaker - Python version of the old FPMaker with export for pastebin.com
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------




import sys, os
import tkinter as tk
from tkinter import ttk
import configparser
import datetime
from time import sleep

from gui import Gui
from importer import Importer
from fpchart import Character, Chart
from exporter import Exporter

class FPMaker:
    def __init__(self):
        """
        Constructor
        """
        config = configparser.ConfigParser()
        config.read("config.ini")
        
        root = tk.Tk()
        self.gui = Gui(root)
        self.gui.master.title("FPMaker")
        self.gui.menuFile.add_command(label="Open", command=self.gui.openFile)
        self.gui.menuFile.add_command(label="Save", command=self.gui.saveFile)
        self.gui.menuFile.add_command(label="Export", command=self.export)
        self.gui.menuFile.add_separator()
        self.gui.menuFile.add_command(label="Exit", command=self.stop)
        self.gui.pack(side="left", fill="both", expand=True)
        self.importer = Importer()
        self.fpchart = Chart()
        self.exporter = Exporter(developerAPIKey=config["pastebin.com"]["DeveloperAPIKey"], expireDate=config["pastebin.com"]["ExpireDate"])
        self.guilds = []
        self.gui.recruit.button.config(command=self.parseRecruit)
        self.gui.importer.button.config(command=self.parseImport)
        self.gui.importer.button.config(command=self.parseImport)
        
    def start(self):
        """
        Main loop
        """
        self.gui.mainloop()


    def stop(self):
        """
        Quit
        """
        self.gui.statusbar["text"] = "Goodbye!"
        sleep(1)
        self.gui.quit()
        exit(0)

    def parseImport(self):
        data = self.importer.parse(self.gui.importer.importData.get())

        # when the data already exists, do update by removing...
        try:
            self.guilds.remove(data[0])
        except:
            pass
        for m in self.fpchart.members:            
            self.fpchart.members = [x for x in self.fpchart.members if x.guild != data[0]]
        
        # ...and adding
        self.guilds.append(data[0])
        for m in data[1]:
            member = Character(m[0], m[1], m[2], m[3], data[0])
            self.fpchart.addPlayer(member)
        
        self.gui.texttable.editText(method="erase")
        self.gui.texttable.editText(self.fpchart.getChartTextMode())
        self.gui.spreadsheettable.editText(method="erase")
        self.gui.spreadsheettable.editText(self.fpchart.getChartSpreadsheetMode())
        
    
    def parseRecruit(self):
        guild = "Recruit"
        member = Character(self.gui.recruit.ignData.get(), self.gui.recruit.levelData.get(), self.gui.recruit.iouscoreData.get(), self.gui.recruit.networthData.get())
        
        # when the data already exists, do update by removing...
        for m in self.fpchart.members:            
            self.fpchart.members = [x for x in self.fpchart.members if x.ign != member.ign]
        
        # ...and adding
        self.fpchart.addPlayer(member)
        
        self.gui.texttable.editText(method="erase")
        self.gui.spreadsheettable.editText(method="erase")
        self.gui.texttable.editText(self.fpchart.getChartTextMode())
        self.gui.spreadsheettable.editText(self.fpchart.getChartSpreadsheetMode())
        
        
    def export(self):
        self.gui.statusbar["text"] = "Exporting to pastebin.org..."
        title = "FP Table {}".format(datetime.datetime.utcnow().strftime("%Y-%m-%d"))
        
        contentsText = self.gui.texttable.getContents()
        responseSuccess = self.exporter.export(paste=contentsText, title=title)
        if responseSuccess:
            message = "Export FP Table complete, link: {}".format(responseSuccess[2])
        else:
            message = "Something went wrong...({})".format(self.exporter.response)
        self.gui.links.editText("\n\n--------------------------------------------------------------------------------\n\n")
        self.gui.links.editText(message, method="add")
        self.gui.statusbar["text"] = message
        
        contentsSpreadsheet = self.gui.spreadsheettable.getContents()
        responseSuccess = self.exporter.export(paste=contentsSpreadsheet, title=title)
        if responseSuccess:
            message = "Export Spreadsheet complete, link: {}".format(responseSuccess[1])
        else:
            message = "Something went wrong...({})".format(self.exporter.response)
        self.gui.links.editText("\n\n--------------------------------------------------------------------------------\n\n")
        self.gui.links.editText(message, method="add")
        self.gui.statusbar["text"] = message




if __name__ == "__main__":
    app = FPMaker()
    app.start()

