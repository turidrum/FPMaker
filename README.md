# FPMaker

Python version of the old [FPMaker](legacy/FPMaker.html) with export for pastebin.


## License

This program is free software and released under [GPLv2](LICENSE.md).
