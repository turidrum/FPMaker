#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   FPMaker - Python version of the old FPMaker with export for pastebin.com
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------




import urllib.parse
import urllib.request



class Exporter:
    def __init__(self, developerAPIKey=None, expireDate="1Y"):

        self.developerAPIKey = developerAPIKey
        self.url = "https://pastebin.com/api/api_post.php"
        self.response = ""
        self.exported = []
        self.expireDate = expireDate
        
    
    def export(self, paste, title="FP Table"):
        if not self.developerAPIKey:
            raise RuntimeError("No developer API key found.")
        
        if paste:
            api = {
                "api_option": "paste",
                "api_dev_key": self.developerAPIKey,
                "api_paste_name": title,
                "api_paste_code": paste,
                "api_paste_private": 0, # 0=public 1=unlisted
                                        # 2=private, note that for
                                        # free accounts there is a
                                        # limit of 25 pastes per day
                                        # for unlisted, and 10 for
                                        # private. it means that for
                                        # the purpose of this program,
                                        # unlisted is the best option,
                                        # but for testing is better to
                                        # use public and lowering the
                                        # expiring data
                # N = Never
                # 10M = 10 Minutes
                # 1H = 1 Hour
                # 1D = 1 Day
                # 1W = 1 Week
                # 2W = 2 Weeks
                # 1M = 1 Month
                # 6M = 6 Months
                # 1Y = 1 Year 
                "api_paste_expire_date": self.expireDate,
                "api_paste_format": "text",
                "api_user_key": ""
            }
            data = urllib.parse.urlencode(api)
            data = data.encode("utf-8")
            print("")
            req = urllib.request.Request(self.url, data)
            
            with urllib.request.urlopen(req) as response:
                self.response = response.read().decode("utf-8")
                if self.response[:21] == "https://pastebin.com/":
                    lastExportRawURL = "https://pastebin.com/raw/{}".format(self.response[21:])
                    lastExportURL = self.response
                    self.exported.append([title, lastExportURL, lastExportRawURL])
                    return [title, lastExportURL, lastExportRawURL]
        return None
                


if __name__ == "__main__":
    import configparser

    config = configparser.ConfigParser()
    config.read("config.ini")
    if "pastebin.com" in config:
        exporter = Exporter(config["pastebin.com"]["DeveloperAPIKey"])
        if exporter.export("This is a test."):
            print(exporter.response)
        else:
            print("Something went wrong...({})".format(exporter.response))
    else:
        print("Sorry, configure your program first.")

    
        
