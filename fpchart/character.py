#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   FPMaker - Python version of the old FPMaker with export for pastebin.com
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------




class Character:
    def __init__(self, ign, level, iouscore, networth, guild="Recruit"):
        self.ign = ign
        self.level = int(level)
        self.iouscore = int(iouscore)
        self.networth = int(networth)
        self.guild = guild
        self.personalEffort = self.getPersonalEffort(self.iouscore, self.level)
        self.familyPoints = self.getFP(self.level, self.iouscore, self.networth, self.personalEffort)
        self.guildInitials = self.getGuildInitials(self.guild)
        self.ranks = {"family":0, "effort":0, "level":0}
        self.balance = "Balanced"

    def getPersonalEffort(self, iouscore, level):
        """
        calcs the personal effort, it is the ratio between IOU Score and the character level

        @param iouscore                         IOU Score
        @param level                            character level
        @return                                 number representing the personal effort
        """

        if level == 0:
            return 1
        else:
            return iouscore / level

    
    def getFP(self, level, iouscore, networth, personalEffort):
        """
        calcs the Family Points

        @param level                            character level
        @param iouscore                         IOU Score
        @param networth                         total stones invested
        @param personalEffort                   personal effort
        @return                                 number representing the Family Points
        """
        
        return int(personalEffort*networth*iouscore/1000000000)


    def updateBalance(self):
        """updates the character balance, in graphical versions of the FP
        tables it is the color of the row, anyway, it represent the
        effort put toward the family or personal, by comparing the
        family rank and the average of the other two ranks
        """
        
        personal = int((self.ranks["effort"] + self.ranks["level"]) / 2)
        family = self.ranks["family"]
        balance = ""
        if family - personal < 0:
            balance = "Family{}".format("+"*(int((personal-family)/5)))
        elif personal - family < 0:
            balance = "Personal{}".format("+"*(int((family-personal)/5)))
        else:
            balance = "Balanced"
        self.balance = balance
        
        
    
    def getGuildInitials(self, guild):
        """calcs the initials from a full guild name, if there are no spaces
        separating the words, it relies on the upper case characters

        @param guild                            name of the guild
        @return                                 guild initials
        """

        
        shorten = ""
        if " " in guild and guild.index(" ") >= 0:
            first = True
            for l in guild:
                if first:
                    first = False
                    shorten += l.upper()
                else:
                    if l == " ":
                        first = True
        else:
            for l in guild:
                if l != l.lower():
                    shorten += l.upper()

        return shorten
    
    def printSummary(self):
        """
        print a summary for this character
        """

        print("IGN: {:<20}".format(self.ign))
        print("Level: {:<20}".format(self.level))
        print("IOU Score: {:<20}".format(self.iouscore))
        print("Networth: {:<20}".format(self.networth))
        print("Personal Effort: {:<20}".format(self.personalEffort))
        print("Family Points: {:<20}".format(self.familyPoints))
        print("Guild: {:<20}".format(self.guild))
        print("Guild Initials: {:<20}".format(self.guildInitials))
        print("Family Rank: {:<20}".format(self.ranks["family"]))
        print("Effort Rank: {:<20}".format(self.ranks["effort"]))
        print("Level Rank: {:<20}".format(self.ranks["level"]))




if __name__ == "__main__":
    brutalboy = Character("brutalboy", 1954, 391933, 219586000000, "Holy Warriors")
    brutalboy.printSummary()

    turidrum = Character("turidrum", 1954, 391933, 219586000000, "NoMatterTheCost")
    turidrum.printSummary()
    
    unknown1 = Character("unknown1", 1954, 391933, 239586000000)
    unknown.printSummary()

    unknown2 = Character("unknown2", 1464, 243145, 65456000000)
    unknown.printSummary()

    unknown3 = Character("unknown3", 634, 12534, 5195860000)
    unknown.printSummary()

    unknown4 = Character("unknown4", 754, 1341, 164586000)
    unknown.printSummary()

