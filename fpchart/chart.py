#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   FPMaker - Python version of the old FPMaker with export for pastebin.com
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------




class Chart:
    
    def __init__(self):
        """
        Constructor
        """
        
        self.members = []
        
    
    def addPlayer(self, player):
        """
        Adds a player to the chart

        @param player                           player, instance of Character
        """
        
        self.members.append(player)
    
    
    def sortChart(self):
        """
        Sorts the members for the Family Points chart, also assign family, effor and level ranks
        """

        chart = []
        # sort the members list based on level and add the level rank
        self.members = sorted(self.members, key=lambda m:m.level, reverse=True)
        c = 0
        for m in self.members:
            c += 1
            m.ranks["level"] = c
            
        # sort the members list based on personal effort and add the effort rank
        self.members = sorted(self.members, key=lambda m:m.personalEffort, reverse=True)
        c = 0
        for m in self.members:
            c += 1
            m.ranks["effort"] = c
            
        # sort the members list based on family points and add the family rank, also update the balance(in graphical versions it was the color, red and green)
        self.members = sorted(self.members, key=lambda m:m.familyPoints, reverse=True)
        c = 0
        for m in self.members:
            c += 1
            m.ranks["family"] = c
            m.updateBalance()
        
        
    
    def getChartTextMode(self):
        """
        Makes the chart in plain text
        
        @return                                 string containing the chart
        """
        
        self.sortChart()

        padding = 1
        
        # headers and the minimum required width for their columns
        headers = [["Balance",0],["Family Rank",0], ["Effort Rank",0], ["Level Rank",0], ["Guild",0], ["Member",0], ["Level",0], ["IOU Score",0], ["Personal Effort",0], ["Net Worth",0], ["Family Points",0]]
        
        headers[0][1] = max([len(headers[0][0]), len(str(max(self.members, key=lambda m:len(str(m.balance))).balance))])
        headers[1][1] = max([len(headers[1][0]), len("#{:,}".format(max(self.members, key=lambda m:len(str(m.ranks["family"]))).ranks["family"]))])
        headers[2][1] = max([len(headers[2][0]), len("#{:,}".format(max(self.members, key=lambda m:len(str(m.ranks["effort"]))).ranks["effort"]))])
        headers[3][1] = max([len(headers[3][0]), len("#{:,}".format(max(self.members, key=lambda m:len(str(m.ranks["level"]))).ranks["level"]))])
        headers[4][1] = max([len(headers[4][0]), len(str(max(self.members, key=lambda m:len(str(m.guildInitials))).guildInitials))])
        headers[5][1] = max([len(headers[5][0]), len(str(max(self.members, key=lambda m:len(str(m.ign))).ign))])
        headers[6][1] = max([len(headers[6][0]), len("{:,}".format(max(self.members, key=lambda m:len(str(m.level))).level))])
        headers[7][1] = max([len(headers[7][0]), len("{:,}".format(max(self.members, key=lambda m:len(str(m.iouscore))).iouscore))])
        headers[8][1] = max([len(headers[8][0]), len("{:,.2f}".format(max(self.members, key=lambda m:len(str(m.personalEffort))).personalEffort))])
        headers[9][1] = max([len(headers[9][0]), len("{:,}".format(max(self.members, key=lambda m:len(str(m.networth))).networth))])
        headers[10][1] = max([len(headers[10][0]), len("{:,}".format(max(self.members, key=lambda m:len(str(m.familyPoints))).familyPoints))])
        
        chart = ""
        rowSeparator = "+"
        rowSeparator += "-"*(headers[0][1]+(padding*2))
        rowSeparator += "+"
        rowSeparator += "-"*(headers[1][1]+(padding*2))
        rowSeparator += "+"
        rowSeparator += "-"*(headers[2][1]+(padding*2))
        rowSeparator += "+"
        rowSeparator += "-"*(headers[3][1]+(padding*2))
        rowSeparator += "+"
        rowSeparator += "-"*(headers[4][1]+(padding*2))
        rowSeparator += "+"
        rowSeparator += "-"*(headers[5][1]+(padding*2))
        rowSeparator += "+"
        rowSeparator += "-"*(headers[6][1]+(padding*2))
        rowSeparator += "+"
        rowSeparator += "-"*(headers[7][1]+(padding*2))
        rowSeparator += "+"
        rowSeparator += "-"*(headers[8][1]+(padding*2))
        rowSeparator += "+"
        rowSeparator += "-"*(headers[9][1]+(padding*2))
        rowSeparator += "+"
        rowSeparator += "-"*(headers[10][1]+(padding*2))
        rowSeparator += "+\n"
        
        chart += rowSeparator
        
        chart += "|" + (" "*padding) + headers[0][0] + (" "*(padding + (headers[0][1] - len(str(headers[0][0])))))
        chart += "|" + (" "*padding) + headers[1][0] + (" "*(padding + (headers[1][1] - len(str(headers[1][0])))))
        chart += "|" + (" "*padding) + headers[2][0] + (" "*(padding + (headers[2][1] - len(str(headers[2][0])))))
        chart += "|" + (" "*padding) + headers[3][0] + (" "*(padding + (headers[3][1] - len(str(headers[3][0])))))
        chart += "|" + (" "*padding) + headers[4][0] + (" "*(padding + (headers[4][1] - len(str(headers[4][0])))))
        chart += "|" + (" "*padding) + headers[5][0] + (" "*(padding + (headers[5][1] - len(str(headers[5][0])))))
        chart += "|" + (" "*padding) + headers[6][0] + (" "*(padding + (headers[6][1] - len(str(headers[6][0])))))
        chart += "|" + (" "*padding) + headers[7][0] + (" "*(padding + (headers[7][1] - len(str(headers[7][0])))))
        chart += "|" + (" "*padding) + headers[8][0] + (" "*(padding + (headers[8][1] - len(str(headers[8][0])))))
        chart += "|" + (" "*padding) + headers[9][0] + (" "*(padding + (headers[9][1] - len(str(headers[9][0])))))
        chart += "|" + (" "*padding) + headers[10][0] + (" "*(padding + (headers[10][1] - len(str(headers[10][0])))))
        chart += "|\n"
        chart += rowSeparator
        
        for m in self.members:
            chart += "|{0}{1}{fill}{0}".format(" "*padding, m.balance, fill=" "*(headers[0][1] - len(m.balance)))
            chart += "|{0}{fill}#{1:,}{0}".format(" "*padding, m.ranks["family"], fill=" "*(headers[1][1] - len("#{:,}".format(m.ranks["family"]))))
            chart += "|{0}{fill}#{1:,}{0}".format(" "*padding, m.ranks["effort"], fill=" "*(headers[2][1] - len("#{:,}".format(m.ranks["effort"]))))
            chart += "|{0}{fill}#{1:,}{0}".format(" "*padding, m.ranks["level"], fill=" "*(headers[3][1] - len("#{:,}".format(m.ranks["level"]))))
            chart += "|{0}{1}{fill}{0}".format(" "*padding, m.guildInitials, fill=" "*(headers[4][1] - len(m.guildInitials)))
            chart += "|{0}{1}{fill}{0}".format(" "*padding, m.ign, fill=" "*(headers[5][1] - len(m.ign)))
            chart += "|{0}{fill}{1:,}{0}".format(" "*padding, m.level, fill=" "*(headers[6][1] - len("{:,}".format(m.level))))
            chart += "|{0}{fill}{1:,}{0}".format(" "*padding, m.iouscore, fill=" "*(headers[7][1] - len("{:,}".format(m.iouscore))))
            chart += "|{0}{fill}{1:,.2f}{0}".format(" "*padding, m.personalEffort, fill=" "*(headers[8][1] - len("{:,.2f}".format(m.personalEffort))))
            chart += "|{0}{fill}{1:,}{0}".format(" "*padding, m.networth, fill=" "*(headers[9][1] - len("{:,}".format(m.networth))))
            chart += "|{0}{fill}{1:,}{0}".format(" "*padding, m.familyPoints, fill=" "*(headers[10][1] - len("{:,}".format(m.familyPoints))))
            chart += "|\n"
            chart += rowSeparator

        return chart
    
    
    def getChartSpreadsheetMode(self):
        """Makes the chart in plain text for the spreadsheet, with tabs as
        field separators and new lines as rows
        
        @return                                 string containing the chart
        
        """
        
        self.sortChart()
                
        # column headers
        headers = ["Balance", "Family Rank", "Effort Rank", "Level Rank", "Guild", "Member", "Level", "IOU Score", "Personal Effort", "Net Worth", "Family Points"]
        
        chart = "{}\n".format("\t".join(headers))
        
        for m in self.members:
            fields = [
                m.balance,
                "#{:,}".format(m.ranks["family"]),
                "#{:,}".format(m.ranks["effort"]),
                "#{:,}".format(m.ranks["level"]),
                m.guildInitials,
                m.ign,
                "{:,}".format(m.level),
                "{:,}".format(m.iouscore),
                "{:,.2f}".format(m.personalEffort),
                "{:,}".format(m.networth),
                "{:,}".format(m.familyPoints)
            ]
            chart += "{}\n".format("\t".join(fields))
        
        return chart

    
    
    def printSummary(self):
        """
        Prints a summary for each player in the chart
        """
        
        for m in self.members:
            m.printSummary()
            print("-"*80)
    
    

if __name__ == "__main__":
    from character import Character
    chart = Chart()
    chart.addPlayer(Character("brutalboy", 1954, 391933, 219586000000, "Holy Warriors"))
    chart.addPlayer(Character("turidrum", 1954, 391933, 219586000000, "NoMatterTheCost"))
    chart.addPlayer(Character("unknown1", 1954, 391933, 239586000000))
    chart.addPlayer(Character("unknown2", 1464, 243145, 65456000000))
    chart.addPlayer(Character("unknown3", 611, 12534, 5195860000))
    chart.addPlayer(Character("unknown4", 756, 1341, 16458134000))
    chart.addPlayer(Character("unknown5", 1514, 391933, 239586000000))
    chart.addPlayer(Character("unknown6", 1514, 243145, 654561345000))
    chart.addPlayer(Character("unknown7", 645, 12534, 519585000))
    chart.addPlayer(Character("unknown8", 751, 1341, 16445100))
    chart.addPlayer(Character("unknown9", 1914, 391933, 2395145000000))
    chart.addPlayer(Character("unknown10", 1564, 243145, 65454000000))
    chart.addPlayer(Character("unknown11", 654, 12534, 5195861345000))
    chart.addPlayer(Character("unknown12", 744, 1341, 16458134500))
    chart.addPlayer(Character("unknown13", 11, 38, 23951345000000))
    chart.addPlayer(Character("unknown14", 464, 24315, 6545500000))
    chart.addPlayer(Character("unknown15", 654, 12534, 5195845000))
    chart.addPlayer(Character("unknown16", 794, 1341, 164586000))
    chart.addPlayer(Character("unknown17", 1754, 391933, 239346000000))
    chart.addPlayer(Character("unknown18", 1964, 243145, 65456000000))
    chart.addPlayer(Character("unknown19", 674, 12554, 51958656000))
    chart.addPlayer(Character("unknown20", 744, 13711, 16458670))
    chart.addPlayer(Character("unknown21", 1354, 395933, 237586000000))
    chart.addPlayer(Character("unknown22", 1264, 243145, 6556000000))
    chart.addPlayer(Character("unknown23", 634, 12354, 517860000))
    chart.addPlayer(Character("unknown24", 1754, 1351, 161586000))

    chart.printSummary()
    print(chart.getChartTextMode())
    print(chart.getChartSpreadsheetMode())
