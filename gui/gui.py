#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   FPMaker - Python version of the old FPMaker with export for pastebin.com
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------





import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as mbox
from gui.banner import Banner
from gui.recruit import Recruit
from gui.importer import Importer
from gui.texttable import TextTable



class Gui(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
        """
        Constructor
        """
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        
        self.mainMenu = tk.Menu(parent)
        self.master.config(menu=self.mainMenu)
        self.menuFile = tk.Menu(self.mainMenu)
        self.mainMenu.add_cascade(label="File", menu=self.menuFile)
        
        self.banner = Banner(self)
        self.banner.loadImage("assets/hwf-logo.png")
        self.banner.pack(side="top", fill="x", expand=False)
        
        inputContainer = ttk.Frame(self)
        inputContainer.rowconfigure(0, weight=1)
        inputContainer.columnconfigure(0, weight=1)
        inputContainer.columnconfigure(1, weight=1)
        self.recruit = Recruit(inputContainer)
        self.recruit.grid(row=0, column=0, columnspan=1, sticky="WENS")
        
        self.importer = Importer(inputContainer)
        self.importer.grid(row=0, column=1, columnspan=1, sticky="WENS")
        inputContainer.pack(side="top", fill="x", expand=False)

        notebook = ttk.Notebook(self)
        notebook.pack(side="top", fill="both", expand=True)
        self.texttable = TextTable(self)
        self.texttable.pack(side="top", fill="both", expand=True)
        self.spreadsheettable = TextTable(self)
        self.spreadsheettable.pack(side="top", fill="both", expand=True)
        self.links = TextTable(self)
        self.links.pack(side="top", fill="both", expand=True)

        notebook.add(self.texttable, text="FP Table")
        notebook.add(self.spreadsheettable, text="Spreadsheet")
        notebook.add(self.links, text="Links")

        self.statusbar = ttk.Label(self, text="Welcome")
        self.statusbar.config(anchor="w", borderwidth=1, relief="sunken")
        self.statusbar.pack(side="bottom", fill="x", expand=False)

        
        
    def openFile(self):
        msg = """An Error occurred:\n\nImpossible to open a file, this function is not yet implemented.
        """
        mbox.showerror(title="Error", message=msg)
        
    def saveFile(self):
        msg = """An Error occurred:\n\nImpossible to save the file, this function is not yet implemented.
        """
        mbox.showerror(title="Error", message=msg)
        
    def exportData(self):
        msg = """An Error occurred:\n\nImpossible to export data, this function is not yet implemented.
        """
        mbox.showerror(title="Error", message=msg)
            
            
        
        
    def selectAll(self, event):
        """
        selects the text in a widget
        
        @param event                    tk event
        """
        if __name__ != "__main__":
            self.parent.selectAll()
        else:
            event.widget.tag_add("sel","1.0","end")

    def editText(self, target, text, method = "add"):
        """
        edit a Text widget in two modes, add and erase
        
        @param target                   the widget to edit
        @param text                     the text to be edit
        @param method                   type of edit, can be add or erase
        """
        
        if method == "erase":
            target.delete(1.0, Tkinter.END)
        elif method == "add":
            target.insert(tk.INSERT, "%s\n" % text)
        

if __name__ == "__main__":
    root = tk.Tk()
    gui = Gui(root)
    gui.pack(side="left", fill="both", expand=True)
    
    root.mainloop()
