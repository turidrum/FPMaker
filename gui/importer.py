#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   FPMaker - Python version of the old FPMaker with export for pastebin.com
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------




import tkinter as tk
from tkinter import ttk


class Importer(ttk.LabelFrame):
    def __init__(self, parent, *args, **kwargs):
        """
        Constructor
        """
        ttk.LabelFrame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.config(text="Import from game")
        entries = ["Paste the result of /export"]
        self.form = []
        self.makeForm(self, entries)
        for r in range(0, len(entries)):
            self.rowconfigure(r, weight=1)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.importData = tk.StringVar()
        self.form[0][1].config(textvariable=self.importData)
        
        
    def makeForm(self, parent, fields):
        c = 0
        pad = 4
        
        for field in fields:
            label = ttk.Label(parent, text=field)
            label.grid(in_=parent, row=c, column=0, padx=pad, pady=pad, ipadx=pad, ipady=pad, sticky="E")
            
            entry = ttk.Entry(parent)
            entry.grid(in_=parent, row=c, column=1, padx=pad, pady=pad, ipadx=pad, ipady=pad, sticky="W")
            entry.bind("<Control-KeyRelease-a>", self.selectAll)
            
            self.form.append((label, entry))
            c += 1
        self.button = ttk.Button(parent, text="Add this guild")
        self.button.grid(in_=parent, row=len(fields), column=0, columnspan=2, padx=pad, pady=pad, ipadx=pad, ipady=pad, sticky="WENS")

            
    def selectAll(self, event):
        """
        selects the text in a widget
        
        @param event                    tk event
        """
        event.widget.select_range(0, tk.END)
        event.widget.icursor(tk.END)
	

if __name__ == "__main__":
    root = tk.Tk()
    r = Importer(root)
    r.pack(side="top", fill="both", expand=True)
    
    root.mainloop()
