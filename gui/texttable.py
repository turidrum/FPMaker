#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   FPMaker - Python version of the old FPMaker with export for pastebin.com
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------




import tkinter as tk
from tkinter import ttk


class TextTable(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
        """
        Constructor
        """
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.parent = parent
        self.scrollbarV = ttk.Scrollbar(self)
        self.scrollbarH = ttk.Scrollbar(self)
        self.text = tk.Text(self)
        self.text.config(yscrollcommand=self.scrollbarV.set, xscrollcommand=self.scrollbarH.set, wrap=tk.NONE)
        self.scrollbarV.config(orient=tk.VERTICAL, command=self.text.yview)
        self.scrollbarH.config(orient=tk.HORIZONTAL, command=self.text.xview)
        self.master.bind_class("Text","<Control-KeyRelease-a>", self.selectAll)
        self.text.grid(column=0, row=0, columnspan=1, sticky="NSWE")
        self.scrollbarV.grid(column=1, row=0, columnspan=1, sticky="NS")
        self.scrollbarH.grid(column=0, row=1, columnspan=1, sticky="WE")
        text = "Holy Warriors Family"
        self.editText(text)
        
    def selectAll(self, event):
        """
        selects the text in a widget
        
        @param event                    tk event
        """
        
        event.widget.tag_add("sel","1.0","end")

    def editText(self, text = "", method = "add"):
        """
        edit the Text widget in two modes, add and erase
        
        @param text                     the text to be edit
        @param method                   type of edit, can be add(default) or erase
        """
        
        target = self.text
        if method == "erase":
            target.delete(1.0, tk.END)
        elif method == "add":
            target.insert(tk.INSERT, "%s\n" % text)

    def getContents(self):
        """
        Retrieves the contents of the text widget
        
        @return                         string representing the contents of the widget
        """

        return self.text.get(1.0, tk.END)
        

if __name__ == "__main__":
    root = tk.Tk()
    tt = TextTable(root)
    tt.pack(side="top", fill="both", expand=True)
    
    root.mainloop()
