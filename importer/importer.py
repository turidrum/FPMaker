#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# ------------------------------------------------------------------------------
#
#   FPMaker - Python version of the old FPMaker with export for pastebin.com
#   Copyright (C) 2018 turidrum
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation; either version 2 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
#   02110-1301, USA.
#
# ------------------------------------------------------------------------------




import atexit
import os
import readline
import math
import json



class Importer:

    def __init__(self):
        pass
    
    
    def parse(self, text=""):
        """
        Parses the data received from game using the command /export

        @return                                 list containing the guild name and a list of members
        """
        data = ""
        if text == "":
            data = json.loads(input("Paste here the data collected in game using the console command /export: "))
        else:
            data = json.loads(text)
        members = []
        for m in data["guildData"]["members"]:
            name = m["name"]
            level = m["level"]
            iouscore = m["iou"]
            networth = m["networth"]
            members.append([name, level, iouscore, networth])
        
        guild = data["guildData"]["name"]
        return [guild, members];
    
    
    
if __name__ == "__main__":
    i = Importer()
    data = i.parse()
    print(data)


